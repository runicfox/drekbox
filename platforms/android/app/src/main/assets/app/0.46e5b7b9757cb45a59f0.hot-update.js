webpackHotUpdate(0,{

/***/ "./home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<TabView androidTabsPosition=\"top\">\r\n\t<page-router-outlet *tabItem=\"{title: 'Attributes'}\" name=\"attributesTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Skills'}\" name=\"skillsTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Powers'}\" name=\"powersTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Cyber'}\" name=\"cyberTab\">\r\n\t</page-router-outlet>\r\n</TabView>\r\n<Tabs>\r\n\t<!-- The bottom tab UI is created via TabStrip (the containier) and TabStripItem (for each tab)-->\r\n\t<TabStrip>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Attributes\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Skills\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Powers\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Cyber\"></Label>\r\n\t\t</TabStripItem>\r\n\t</TabStrip>\r\n\r\n\t<TabContentItem>\r\n\t\t<GridLayout rows=\"auto, *\">\r\n\t\t\t<Label text=\"Attributes\" class=\"h1 text-center\" row=\"0\"></Label>\r\n\t\t\t<ListView [items]=\"characterAttributes\" class=\"list-group\" [itemTemplateSelector]=\"templateSelector\"\r\n\t\t\t\trow=\"1\">\r\n\t\t\t\t<ng-template nsTemplateKey=\"red\" let-item=\"item\" let-i=\"index\">\r\n\t\t\t\t\t<GridLayout columns=\"200, *\" rows=\"auto\">\r\n\t\t\t\t\t\t<Label style=\"padding-left:40\" class=\" h2\" [text]=\"item.Name\" color=\"white\" row=\"index\"\r\n\t\t\t\t\t\t\tcolumn=\"0\"></Label>\r\n\t\t\t\t\t\t<TextField [text]=\"item.Score\" keyboardType=\"phone\" maxLength=\"2\" row=\"index\" column=\"1\" style=\"margin-bottom:15; font-size:24px; text-align: center;\"></TextField>\t\t\t\t\t</GridLayout>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</ListView>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\t<TabContentItem>\r\n\t\t<GridLayout rows=\"auto, *\">\r\n\t\t\t<Label text=\"Skills\" class=\"h1 text-center\" row=\"0\"></Label>\r\n\t\t\t<ListView [items]=\"characterSkills\" class=\"list-group\" [itemTemplateSelector]=\"templateSelector\"\r\n\t\t\t\trow=\"1\">\r\n\t\t\t\t<ng-template nsTemplateKey=\"red\" let-item=\"item\" let-i=\"index\">\r\n\t\t\t\t\t<GridLayout columns=\"200, *\" rows=\"auto\">\r\n\t\t\t\t\t\t<Label style=\"padding-left:40\" class=\" h2\" [text]=\"item.Name\" color=\"white\" row=\"index\"\r\n\t\t\t\t\t\t\tcolumn=\"0\"></Label>\r\n\t\t\t\t\t\t<TextField [text]=\"item.Score\" keyboardType=\"phone\" maxLength=\"2\" row=\"index\" column=\"1\" style=\"margin-bottom:15; font-size:24px; text-align: center;\"></TextField>\t\t\t\t\t</GridLayout>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</ListView>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\t<TabContentItem>\r\n\t\t<GridLayout>\r\n\t\t\t<Label text=\"Powers\" class=\"h2 text-center\"></Label>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\r\n\t<TabContentItem>\r\n\t\t<GridLayout>\r\n\t\t\t<Label text=\"Cyber\" class=\"h2 text-center\"></Label>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n</Tabs>\r\n"

/***/ }),

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Longarms',
                Score: 5
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 4
            },
            {
                Name: 'Strength',
                Score: 2
            },
            {
                Name: 'Agility',
                Score: 5
            },
            {
                Name: 'Logic',
                Score: 6
            },
            {
                Name: 'Willpower',
                Score: 2
            },
            {
                Name: 'Intuition',
                Score: 3
            },
            {
                Name: 'Charisma',
                Score: 2
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 2
            },
        ];
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUuY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDZGQUE2RixvQkFBb0IsK0ZBQStGLGdCQUFnQiwyRkFBMkYsZ0JBQWdCLDJGQUEyRixlQUFlLHF2Q0FBcXZDLGdCQUFnQixvQkFBb0IsOHpCQUE4ekIsZ0JBQWdCLG9CQUFvQixvYzs7Ozs7Ozs7QUNBaGpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNUO1NBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUMxQjtnQkFDQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNSO1NBQ0QsQ0FBQztJQUNILENBQUM7SUFNRCxnQ0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsbUNBQVcsR0FBWDtRQUNDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxJQUFJLENBQUMsUUFBUSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7O2dCQXhEd0MsK0RBQWlCOztJQWdEMUQ7UUFEQywrREFBUyxDQUFDLHlGQUFzQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2tDQUM3Qix5RkFBc0I7MERBQUM7SUF6RG5DLGFBQWE7UUFMekIsK0RBQVMsQ0FBQztZQUVWLG9GQUFrQzs7U0FFbEMsQ0FBQzt5Q0FVd0MsK0RBQWlCO09BVDlDLGFBQWEsQ0FrRXpCO0lBQUQsb0JBQUM7Q0FBQTtBQWxFeUIiLCJmaWxlIjoiMC40NmU1YjdiOTc1N2NiNDVhNTlmMC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSBcIjxUYWJWaWV3IGFuZHJvaWRUYWJzUG9zaXRpb249XFxcInRvcFxcXCI+XFxyXFxuXFx0PHBhZ2Utcm91dGVyLW91dGxldCAqdGFiSXRlbT1cXFwie3RpdGxlOiAnQXR0cmlidXRlcyd9XFxcIiBuYW1lPVxcXCJhdHRyaWJ1dGVzVGFiXFxcIj5cXHJcXG5cXHQ8L3BhZ2Utcm91dGVyLW91dGxldD5cXHJcXG5cXHJcXG5cXHQ8cGFnZS1yb3V0ZXItb3V0bGV0ICp0YWJJdGVtPVxcXCJ7dGl0bGU6ICdTa2lsbHMnfVxcXCIgbmFtZT1cXFwic2tpbGxzVGFiXFxcIj5cXHJcXG5cXHQ8L3BhZ2Utcm91dGVyLW91dGxldD5cXHJcXG5cXHJcXG5cXHQ8cGFnZS1yb3V0ZXItb3V0bGV0ICp0YWJJdGVtPVxcXCJ7dGl0bGU6ICdQb3dlcnMnfVxcXCIgbmFtZT1cXFwicG93ZXJzVGFiXFxcIj5cXHJcXG5cXHQ8L3BhZ2Utcm91dGVyLW91dGxldD5cXHJcXG5cXHJcXG5cXHQ8cGFnZS1yb3V0ZXItb3V0bGV0ICp0YWJJdGVtPVxcXCJ7dGl0bGU6ICdDeWJlcid9XFxcIiBuYW1lPVxcXCJjeWJlclRhYlxcXCI+XFxyXFxuXFx0PC9wYWdlLXJvdXRlci1vdXRsZXQ+XFxyXFxuPC9UYWJWaWV3PlxcclxcbjxUYWJzPlxcclxcblxcdDwhLS0gVGhlIGJvdHRvbSB0YWIgVUkgaXMgY3JlYXRlZCB2aWEgVGFiU3RyaXAgKHRoZSBjb250YWluaWVyKSBhbmQgVGFiU3RyaXBJdGVtIChmb3IgZWFjaCB0YWIpLS0+XFxyXFxuXFx0PFRhYlN0cmlwPlxcclxcblxcdFxcdDxUYWJTdHJpcEl0ZW0+XFxyXFxuXFx0XFx0XFx0PExhYmVsIHRleHQ9XFxcIkF0dHJpYnV0ZXNcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0PC9UYWJTdHJpcEl0ZW0+XFxyXFxuXFx0XFx0PFRhYlN0cmlwSXRlbT5cXHJcXG5cXHRcXHRcXHQ8TGFiZWwgdGV4dD1cXFwiU2tpbGxzXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdDwvVGFiU3RyaXBJdGVtPlxcclxcblxcdFxcdDxUYWJTdHJpcEl0ZW0+XFxyXFxuXFx0XFx0XFx0PExhYmVsIHRleHQ9XFxcIlBvd2Vyc1xcXCI+PC9MYWJlbD5cXHJcXG5cXHRcXHQ8L1RhYlN0cmlwSXRlbT5cXHJcXG5cXHRcXHQ8VGFiU3RyaXBJdGVtPlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJDeWJlclxcXCI+PC9MYWJlbD5cXHJcXG5cXHRcXHQ8L1RhYlN0cmlwSXRlbT5cXHJcXG5cXHQ8L1RhYlN0cmlwPlxcclxcblxcclxcblxcdDxUYWJDb250ZW50SXRlbT5cXHJcXG5cXHRcXHQ8R3JpZExheW91dCByb3dzPVxcXCJhdXRvLCAqXFxcIj5cXHJcXG5cXHRcXHRcXHQ8TGFiZWwgdGV4dD1cXFwiQXR0cmlidXRlc1xcXCIgY2xhc3M9XFxcImgxIHRleHQtY2VudGVyXFxcIiByb3c9XFxcIjBcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0XFx0PExpc3RWaWV3IFtpdGVtc109XFxcImNoYXJhY3RlckF0dHJpYnV0ZXNcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwXFxcIiBbaXRlbVRlbXBsYXRlU2VsZWN0b3JdPVxcXCJ0ZW1wbGF0ZVNlbGVjdG9yXFxcIlxcclxcblxcdFxcdFxcdFxcdHJvdz1cXFwiMVxcXCI+XFxyXFxuXFx0XFx0XFx0XFx0PG5nLXRlbXBsYXRlIG5zVGVtcGxhdGVLZXk9XFxcInJlZFxcXCIgbGV0LWl0ZW09XFxcIml0ZW1cXFwiIGxldC1pPVxcXCJpbmRleFxcXCI+XFxyXFxuXFx0XFx0XFx0XFx0XFx0PEdyaWRMYXlvdXQgY29sdW1ucz1cXFwiMjAwLCAqXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXHJcXG5cXHRcXHRcXHRcXHRcXHRcXHQ8TGFiZWwgc3R5bGU9XFxcInBhZGRpbmctbGVmdDo0MFxcXCIgY2xhc3M9XFxcIiBoMlxcXCIgW3RleHRdPVxcXCJpdGVtLk5hbWVcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCIgcm93PVxcXCJpbmRleFxcXCJcXHJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRjb2x1bW49XFxcIjBcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0PFRleHRGaWVsZCBbdGV4dF09XFxcIml0ZW0uU2NvcmVcXFwiIGtleWJvYXJkVHlwZT1cXFwicGhvbmVcXFwiIG1heExlbmd0aD1cXFwiMlxcXCIgcm93PVxcXCJpbmRleFxcXCIgY29sdW1uPVxcXCIxXFxcIiBzdHlsZT1cXFwibWFyZ2luLWJvdHRvbToxNTsgZm9udC1zaXplOjI0cHg7IHRleHQtYWxpZ246IGNlbnRlcjtcXFwiPjwvVGV4dEZpZWxkPlxcdFxcdFxcdFxcdFxcdDwvR3JpZExheW91dD5cXHJcXG5cXHRcXHRcXHRcXHQ8L25nLXRlbXBsYXRlPlxcclxcblxcdFxcdFxcdDwvTGlzdFZpZXc+XFxyXFxuXFx0XFx0PC9HcmlkTGF5b3V0PlxcclxcblxcdDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuXFx0PFRhYkNvbnRlbnRJdGVtPlxcclxcblxcdFxcdDxHcmlkTGF5b3V0IHJvd3M9XFxcImF1dG8sICpcXFwiPlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJTa2lsbHNcXFwiIGNsYXNzPVxcXCJoMSB0ZXh0LWNlbnRlclxcXCIgcm93PVxcXCIwXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdFxcdDxMaXN0VmlldyBbaXRlbXNdPVxcXCJjaGFyYWN0ZXJTa2lsbHNcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwXFxcIiBbaXRlbVRlbXBsYXRlU2VsZWN0b3JdPVxcXCJ0ZW1wbGF0ZVNlbGVjdG9yXFxcIlxcclxcblxcdFxcdFxcdFxcdHJvdz1cXFwiMVxcXCI+XFxyXFxuXFx0XFx0XFx0XFx0PG5nLXRlbXBsYXRlIG5zVGVtcGxhdGVLZXk9XFxcInJlZFxcXCIgbGV0LWl0ZW09XFxcIml0ZW1cXFwiIGxldC1pPVxcXCJpbmRleFxcXCI+XFxyXFxuXFx0XFx0XFx0XFx0XFx0PEdyaWRMYXlvdXQgY29sdW1ucz1cXFwiMjAwLCAqXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXHJcXG5cXHRcXHRcXHRcXHRcXHRcXHQ8TGFiZWwgc3R5bGU9XFxcInBhZGRpbmctbGVmdDo0MFxcXCIgY2xhc3M9XFxcIiBoMlxcXCIgW3RleHRdPVxcXCJpdGVtLk5hbWVcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCIgcm93PVxcXCJpbmRleFxcXCJcXHJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRjb2x1bW49XFxcIjBcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0PFRleHRGaWVsZCBbdGV4dF09XFxcIml0ZW0uU2NvcmVcXFwiIGtleWJvYXJkVHlwZT1cXFwicGhvbmVcXFwiIG1heExlbmd0aD1cXFwiMlxcXCIgcm93PVxcXCJpbmRleFxcXCIgY29sdW1uPVxcXCIxXFxcIiBzdHlsZT1cXFwibWFyZ2luLWJvdHRvbToxNTsgZm9udC1zaXplOjI0cHg7IHRleHQtYWxpZ246IGNlbnRlcjtcXFwiPjwvVGV4dEZpZWxkPlxcdFxcdFxcdFxcdFxcdDwvR3JpZExheW91dD5cXHJcXG5cXHRcXHRcXHRcXHQ8L25nLXRlbXBsYXRlPlxcclxcblxcdFxcdFxcdDwvTGlzdFZpZXc+XFxyXFxuXFx0XFx0PC9HcmlkTGF5b3V0PlxcclxcblxcdDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuXFx0PFRhYkNvbnRlbnRJdGVtPlxcclxcblxcdFxcdDxHcmlkTGF5b3V0PlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJQb3dlcnNcXFwiIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG5cXHRcXHQ8L0dyaWRMYXlvdXQ+XFxyXFxuXFx0PC9UYWJDb250ZW50SXRlbT5cXHJcXG5cXHJcXG5cXHQ8VGFiQ29udGVudEl0ZW0+XFxyXFxuXFx0XFx0PEdyaWRMYXlvdXQ+XFxyXFxuXFx0XFx0XFx0PExhYmVsIHRleHQ9XFxcIkN5YmVyXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0PC9HcmlkTGF5b3V0PlxcclxcblxcdDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuPC9UYWJzPlxcclxcblwiIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXInO1xyXG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyIH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogJ2hvbWUuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWyAnaG9tZS5jb21wb25lbnQuY3NzJyBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRwcml2YXRlIF9tYWluQ29udGVudFRleHQ6IHN0cmluZztcclxuXHJcblx0bW9kaWZpZXI6IG51bWJlcjtcclxuXHRpbml0dmFsdWU6IG51bWJlcjtcclxuXHRwbGF5ZXJOYW1lOiBzdHJpbmc7XHJcblx0Y2hhcmFjdGVyQXR0cmlidXRlcztcclxuXHRjaGFyYWN0ZXJTa2lsbHM7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgX2NoYW5nZURldGVjdGlvblJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYpIHtcclxuXHRcdHRoaXMucGxheWVyTmFtZSA9ICdUb2RkJztcclxuXHRcdHRoaXMuY2hhcmFjdGVyU2tpbGxzID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0xvbmdhcm1zJyxcclxuXHRcdFx0XHRTY29yZTogNVxyXG5cdFx0fV07XHJcblx0XHR0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMgPSBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQm9keScsXHJcblx0XHRcdFx0U2NvcmU6IDRcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdTdHJlbmd0aCcsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdBZ2lsaXR5JyxcclxuXHRcdFx0XHRTY29yZTogNVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0xvZ2ljJyxcclxuXHRcdFx0XHRTY29yZTogNlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1dpbGxwb3dlcicsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdJbnR1aXRpb24nLFxyXG5cdFx0XHRcdFNjb3JlOiAzXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQ2hhcmlzbWEnLFxyXG5cdFx0XHRcdFNjb3JlOiAyXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRXNzZW5jZScsXHJcblx0XHRcdFx0U2NvcmU6IDZcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdFZGdlJyxcclxuXHRcdFx0XHRTY29yZTogMlxyXG5cdFx0XHR9LFxyXG5cdFx0XTtcclxuXHR9XHJcblxyXG5cdEBWaWV3Q2hpbGQoUmFkU2lkZURyYXdlckNvbXBvbmVudCwgeyBzdGF0aWM6IGZhbHNlIH0pXHJcblx0cHVibGljIGRyYXdlckNvbXBvbmVudDogUmFkU2lkZURyYXdlckNvbXBvbmVudDtcclxuXHRwcml2YXRlIGRyYXdlcjogUmFkU2lkZURyYXdlcjtcclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7fVxyXG5cclxuXHRvbkJ1dHRvblRhcCgpOiB2b2lkIHtcclxuXHRcdHRoaXMubW9kaWZpZXIgPSB0eXBlb2YgdGhpcy5tb2RpZmllciA9PT0gJ3VuZGVmaW5lZCcgPyAwIDogdGhpcy5tb2RpZmllcjtcclxuXHRcdHRoaXMuaW5pdHZhbHVlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogTWF0aC5mbG9vcig2KSkgKyAxICsgTnVtYmVyKHRoaXMubW9kaWZpZXIpO1xyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9