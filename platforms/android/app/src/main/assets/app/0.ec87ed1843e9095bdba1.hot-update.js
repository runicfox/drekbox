webpackHotUpdate(0,{

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Longarms',
                Score: 5
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 4
            },
            {
                Name: 'Strength',
                Score: 2
            },
            {
                Name: 'Agility',
                Score: 5
            },
            {
                Name: 'Reaction',
                Score: 5
            },
            {
                Name: 'Logic',
                Score: 6
            },
            {
                Name: 'Willpower',
                Score: 2
            },
            {
                Name: 'Intuition',
                Score: 3
            },
            {
                Name: 'Charisma',
                Score: 2
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 2
            },
        ];
        this.modifier = this.getModifer();
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.prototype.getModifer = function () {
        var reaction = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
        var intuition = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Intuition';
        });
        console.log("reaction");
        console.log(reaction);
        return reaction + intuition;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1NBQ0QsQ0FBQztRQUNGLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUMxQjtnQkFDQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxNQUFNO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1I7U0FDRCxDQUFDO1FBRUYsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQU1ELGdDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQixtQ0FBVyxHQUFYO1FBQ0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLElBQUksQ0FBQyxRQUFRLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFDQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQUc7WUFDN0MsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFVBQVU7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQUc7WUFDOUMsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFdBQVc7UUFDakMsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsT0FBTyxRQUFRLEdBQUcsU0FBUyxDQUFDO0lBQzdCLENBQUM7O2dCQTdFd0MsK0RBQWlCOztJQXVEMUQ7UUFEQywrREFBUyxDQUFDLHlGQUFzQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2tDQUM3Qix5RkFBc0I7MERBQUM7SUFoRW5DLGFBQWE7UUFMekIsK0RBQVMsQ0FBQztZQUVWLG9GQUFrQzs7U0FFbEMsQ0FBQzt5Q0FVd0MsK0RBQWlCO09BVDlDLGFBQWEsQ0F1RnpCO0lBQUQsb0JBQUM7Q0FBQTtBQXZGeUIiLCJmaWxlIjoiMC5lYzg3ZWQxODQzZTkwOTViZGJhMS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXInO1xyXG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyIH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogJ2hvbWUuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWyAnaG9tZS5jb21wb25lbnQuY3NzJyBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRwcml2YXRlIF9tYWluQ29udGVudFRleHQ6IHN0cmluZztcclxuXHJcblx0bW9kaWZpZXI6IG51bWJlcjtcclxuXHRpbml0dmFsdWU6IG51bWJlcjtcclxuXHRwbGF5ZXJOYW1lOiBzdHJpbmc7XHJcblx0Y2hhcmFjdGVyQXR0cmlidXRlcztcclxuXHRjaGFyYWN0ZXJTa2lsbHM7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgX2NoYW5nZURldGVjdGlvblJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYpIHtcclxuXHRcdHRoaXMucGxheWVyTmFtZSA9ICdUb2RkJztcclxuXHRcdHRoaXMuY2hhcmFjdGVyU2tpbGxzID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0xvbmdhcm1zJyxcclxuXHRcdFx0XHRTY29yZTogNVxyXG5cdFx0XHR9XHJcblx0XHRdO1xyXG5cdFx0dGhpcy5jaGFyYWN0ZXJBdHRyaWJ1dGVzID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0JvZHknLFxyXG5cdFx0XHRcdFNjb3JlOiA0XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnU3RyZW5ndGgnLFxyXG5cdFx0XHRcdFNjb3JlOiAyXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQWdpbGl0eScsXHJcblx0XHRcdFx0U2NvcmU6IDVcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdSZWFjdGlvbicsXHJcblx0XHRcdFx0U2NvcmU6IDVcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdMb2dpYycsXHJcblx0XHRcdFx0U2NvcmU6IDZcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdXaWxscG93ZXInLFxyXG5cdFx0XHRcdFNjb3JlOiAyXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnSW50dWl0aW9uJyxcclxuXHRcdFx0XHRTY29yZTogM1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0NoYXJpc21hJyxcclxuXHRcdFx0XHRTY29yZTogMlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0Vzc2VuY2UnLFxyXG5cdFx0XHRcdFNjb3JlOiA2XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRWRnZScsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdF07XHJcblxyXG5cdFx0dGhpcy5tb2RpZmllciA9IHRoaXMuZ2V0TW9kaWZlcigpO1xyXG5cdH1cclxuXHJcblx0QFZpZXdDaGlsZChSYWRTaWRlRHJhd2VyQ29tcG9uZW50LCB7IHN0YXRpYzogZmFsc2UgfSlcclxuXHRwdWJsaWMgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xyXG5cdHByaXZhdGUgZHJhd2VyOiBSYWRTaWRlRHJhd2VyO1xyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG5cdG9uQnV0dG9uVGFwKCk6IHZvaWQge1xyXG5cdFx0dGhpcy5tb2RpZmllciA9IHR5cGVvZiB0aGlzLm1vZGlmaWVyID09PSAndW5kZWZpbmVkJyA/IDAgOiB0aGlzLm1vZGlmaWVyO1xyXG5cdFx0dGhpcy5pbml0dmFsdWUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBNYXRoLmZsb29yKDYpKSArIDEgKyBOdW1iZXIodGhpcy5tb2RpZmllcik7XHJcblx0fVxyXG5cclxuXHRnZXRNb2RpZmVyKCkge1xyXG5cdFx0dmFyIHJlYWN0aW9uID0gdGhpcy5jaGFyYWN0ZXJBdHRyaWJ1dGVzLmZpbmQob2JqID0+IHtcclxuXHRcdFx0ICByZXR1cm4gb2JqLk5hbWUgPT0gJ1JlYWN0aW9uJ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0dmFyIGludHVpdGlvbiA9IHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcy5maW5kKG9iaiA9PiB7XHJcblx0XHRcdCAgcmV0dXJuIG9iai5OYW1lID09ICdJbnR1aXRpb24nXHJcblx0XHR9KTtcclxuXHJcblx0XHRjb25zb2xlLmxvZyhcInJlYWN0aW9uXCIpO1xyXG5cdFx0Y29uc29sZS5sb2cocmVhY3Rpb24pO1xyXG5cdFx0cmV0dXJuIHJlYWN0aW9uICsgaW50dWl0aW9uO1xyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9