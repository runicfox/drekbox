(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[0],{

/***/ "./home/home-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-angular/router");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./home/home.component.ts");



var routes = [
    { path: "", component: _home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./home/home.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n    vertical-align: center; \r\n    font-size: 20;\r\n    margin: 15;\r\n}\r\n\r\n.description-label{\r\n    margin-bottom: 15;\r\n}\r\n\r\n.fa {\r\n\tfont-family: \"FontAwesome,fa-solid-900\";\r\n}\r\n\r\nRadSideDrawer .drawerContentText {\r\n    font-size: 13;\r\n    padding: 10;\r\n}\r\n\r\nRadSideDrawer .drawerContentButton {\r\n    margin: 10;\r\n    horizontal-align: left;\r\n}\r\n\r\n.sideStackLayout {\r\n    background-color: gray;\r\n}\r\n\r\n.sideTitleStackLayout {\r\n    text-align: center;\r\n    vertical-align: center;\r\n}\r\n\r\n.sideLabel {\r\n    padding: 10;\r\n}\r\n\r\n.sideLightGrayLabel {\r\n    background-color: lightgray;\r\n}"

/***/ }),

/***/ "./home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<TabView androidTabsPosition=\"top\">\r\n\t<page-router-outlet *tabItem=\"{title: 'Attributes'}\" name=\"attributesTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Skills'}\" name=\"skillsTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Powers'}\" name=\"powersTab\">\r\n\t</page-router-outlet>\r\n\r\n\t<page-router-outlet *tabItem=\"{title: 'Cyber'}\" name=\"cyberTab\">\r\n\t</page-router-outlet>\r\n</TabView>\r\n<Tabs>\r\n\t<!-- The bottom tab UI is created via TabStrip (the containier) and TabStripItem (for each tab)-->\r\n\t<TabStrip>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Attributes\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Skills\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Powers\"></Label>\r\n\t\t</TabStripItem>\r\n\t\t<TabStripItem>\r\n\t\t\t<Label text=\"Cyber\"></Label>\r\n\t\t</TabStripItem>\r\n\t</TabStrip>\r\n\r\n\t<TabContentItem>\r\n\t\t<GridLayout rows=\"auto, *\">\r\n\t\t\t<Label text=\"Attributes\" class=\"h1 text-center\" row=\"0\"></Label>\r\n\t\t\t<ListView [items]=\"characterAttributes\" class=\"list-group\" [itemTemplateSelector]=\"templateSelector\"\r\n\t\t\t\trow=\"1\">\r\n\t\t\t\t<ng-template nsTemplateKey=\"red\" let-item=\"item\" let-i=\"index\">\r\n\t\t\t\t\t<GridLayout columns=\"200, *\" rows=\"auto\">\r\n\t\t\t\t\t\t<Label style=\"padding-left:40\" class=\" h2\" [text]=\"item.Name\" color=\"white\" row=\"index\"\r\n\t\t\t\t\t\t\tcolumn=\"0\"></Label>\r\n\t\t\t\t\t\t<TextField [text]=\"item.Score\" keyboardType=\"phone\" maxLength=\"2\" row=\"index\" column=\"1\" style=\"margin-bottom:15; font-size:24px; text-align: center;\"></TextField>\t\t\t\t\t</GridLayout>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</ListView>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\t<TabContentItem>\r\n\t\t<GridLayout rows=\"auto, *\">\r\n\t\t\t<Label text=\"Skills\" class=\"h1 text-center\" row=\"0\"></Label>\r\n\t\t\t<ListView [items]=\"characterSkills\" class=\"list-group\" [itemTemplateSelector]=\"templateSelector\"\r\n\t\t\t\trow=\"1\">\r\n\t\t\t\t<ng-template nsTemplateKey=\"red\" let-item=\"item\" let-i=\"index\">\r\n\t\t\t\t\t<GridLayout columns=\"200, *\" rows=\"auto\">\r\n\t\t\t\t\t\t<Label style=\"padding-left:40\" class=\" h2\" [text]=\"item.Name\" color=\"white\" row=\"index\"\r\n\t\t\t\t\t\t\tcolumn=\"0\"></Label>\r\n\t\t\t\t\t\t<TextField [text]=\"item.Score\" keyboardType=\"phone\" maxLength=\"2\" row=\"index\" column=\"1\" style=\"margin-bottom:15; font-size:24px; text-align: center;\"></TextField>\t\t\t\t\t</GridLayout>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</ListView>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\t<TabContentItem>\r\n\t\t<GridLayout>\r\n\t\t\t<Label text=\"Powers\" class=\"h2 text-center\"></Label>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n\r\n\t<TabContentItem>\r\n\t\t<GridLayout>\r\n\t\t\t<Label text=\"Cyber\" class=\"h2 text-center\"></Label>\r\n\t\t</GridLayout>\r\n\t</TabContentItem>\r\n</Tabs>\r\n"

/***/ }),

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Archery',
                Score: 0
            },
            {
                Name: 'Automatics',
                Score: 0
            },
            {
                Name: 'Blades',
                Score: 0
            },
            {
                Name: 'Clubs',
                Score: 0
            },
            {
                Name: 'Exotic Ranged Weapon',
                Score: 0
            },
            {
                Name: 'Heavy Weapons',
                Score: 0
            },
            {
                Name: 'Longarms',
                Score: 0
            },
            {
                Name: 'Pistols',
                Score: 0
            },
            {
                Name: 'Throwing Weapons',
                Score: 0
            },
            {
                Name: 'Unarmed Combat',
                Score: 0
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 1
            },
            {
                Name: 'Strength',
                Score: 1
            },
            {
                Name: 'Agility',
                Score: 1
            },
            {
                Name: 'Reaction',
                Score: 1
            },
            {
                Name: 'Logic',
                Score: 1
            },
            {
                Name: 'Willpower',
                Score: 1
            },
            {
                Name: 'Intuition',
                Score: 1
            },
            {
                Name: 'Charisma',
                Score: 1
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 1
            },
        ];
        this.modifier = this.getModifier();
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.prototype.getModifier = function () {
        var reaction = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
        var intuition = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Intuition';
        });
        return reaction.Score + intuition.Score;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./home/home.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-angular/common");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("nativescript-ui-listview/angular");
/* harmony import */ var nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("nativescript-ui-calendar/angular");
/* harmony import */ var nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("nativescript-ui-chart/angular");
/* harmony import */ var nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("nativescript-ui-dataform/angular");
/* harmony import */ var nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("nativescript-ui-autocomplete/angular");
/* harmony import */ var nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("nativescript-ui-gauge/angular");
/* harmony import */ var nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("nativescript-angular/forms");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("./home/home-routing.module.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("./home/home.component.ts");












var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__["NativeScriptUISideDrawerModule"],
                nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__["NativeScriptUIListViewModule"],
                nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__["NativeScriptUICalendarModule"],
                nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__["NativeScriptUIChartModule"],
                nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__["NativeScriptUIDataFormModule"],
                nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__["NativeScriptUIAutoCompleteTextViewModule"],
                nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__["NativeScriptUIGaugeModule"],
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_10__["HomeRoutingModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__["NativeScriptFormsModule"]
            ],
            declarations: [
                _home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"]
            ],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUtcm91dGluZy5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLmNvbXBvbmVudC5odG1sIiwid2VicGFjazovLy8uL2hvbWUvaG9tZS5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUU4QjtBQUV0QjtBQUVqRCxJQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLDZEQUFhLEVBQUU7Q0FDekMsQ0FBQztBQU1GO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixpQkFBaUI7UUFKN0IsOERBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxPQUFPLEVBQUUsQ0FBQyxvRkFBd0IsQ0FBQztTQUN0QyxDQUFDO09BQ1csaUJBQWlCLENBQUk7SUFBRCx3QkFBQztDQUFBO0FBQUo7Ozs7Ozs7O0FDZDlCLDhCQUE4QiwrQkFBK0IsdUJBQXVCLG1CQUFtQixLQUFLLDJCQUEyQiwwQkFBMEIsS0FBSyxhQUFhLGdEQUFnRCxLQUFLLDBDQUEwQyxzQkFBc0Isb0JBQW9CLEtBQUssNENBQTRDLG1CQUFtQiwrQkFBK0IsS0FBSywwQkFBMEIsK0JBQStCLEtBQUssK0JBQStCLDJCQUEyQiwrQkFBK0IsS0FBSyxvQkFBb0Isb0JBQW9CLEtBQUssNkJBQTZCLG9DQUFvQyxLQUFLLEM7Ozs7Ozs7QUNBbnJCLDZGQUE2RixvQkFBb0IsK0ZBQStGLGdCQUFnQiwyRkFBMkYsZ0JBQWdCLDJGQUEyRixlQUFlLHF2Q0FBcXZDLGdCQUFnQixvQkFBb0IsOHpCQUE4ekIsZ0JBQWdCLG9CQUFvQixvYzs7Ozs7Ozs7QUNBaGpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxRQUFRO2dCQUNkLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxlQUFlO2dCQUNyQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLGtCQUFrQjtnQkFDeEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLEtBQUssRUFBRSxDQUFDO2FBQ1I7U0FDRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQzFCO2dCQUNDLElBQUksRUFBRSxNQUFNO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLE1BQU07Z0JBQ1osS0FBSyxFQUFFLENBQUM7YUFDUjtTQUNELENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBTUQsZ0NBQVEsR0FBUixjQUFrQixDQUFDO0lBRW5CLG1DQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sSUFBSSxDQUFDLFFBQVEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRUQsbUNBQVcsR0FBWDtRQUNDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM3QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksVUFBVTtRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM5QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksV0FBVztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sUUFBUSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ3pDLENBQUM7O2dCQS9Hd0MsK0RBQWlCOztJQTJGMUQ7UUFEQywrREFBUyxDQUFDLHlGQUFzQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2tDQUM3Qix5RkFBc0I7MERBQUM7SUFwR25DLGFBQWE7UUFMekIsK0RBQVMsQ0FBQztZQUVWLG9GQUFrQzs7U0FFbEMsQ0FBQzt5Q0FVd0MsK0RBQWlCO09BVDlDLGFBQWEsQ0F5SHpCO0lBQUQsb0JBQUM7Q0FBQTtBQXpIeUI7Ozs7Ozs7OztBQ1QxQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkQ7QUFDWTtBQUNhO0FBQ0o7QUFDQTtBQUNOO0FBQ007QUFDZ0I7QUFDdEI7QUFDTDtBQUVYO0FBQ1Q7QUFzQmpEO0lBQUE7SUFBMEIsQ0FBQztJQUFkLFVBQVU7UUFwQnRCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsaUdBQThCO2dCQUM5Qiw2RkFBNEI7Z0JBQzVCLDZGQUE0QjtnQkFDNUIsdUZBQXlCO2dCQUN6Qiw2RkFBNEI7Z0JBQzVCLDZHQUF3QztnQkFDeEMsdUZBQXlCO2dCQUN6QixvRkFBd0I7Z0JBQ3hCLHVFQUFpQjtnQkFDakIsa0ZBQXVCO2FBQzFCO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLDhEQUFhO2FBQ2hCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLDhEQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxVQUFVLENBQUk7SUFBRCxpQkFBQztDQUFBO0FBQUoiLCJmaWxlIjoiMC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVzIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSBcIi4vaG9tZS5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xyXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IEhvbWVDb21wb25lbnQgfVxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXHJcbiAgICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9tZVJvdXRpbmdNb2R1bGUgeyB9XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gXCIuaG9tZS1wYW5lbHtcXHJcXG4gICAgdmVydGljYWwtYWxpZ246IGNlbnRlcjsgXFxyXFxuICAgIGZvbnQtc2l6ZTogMjA7XFxyXFxuICAgIG1hcmdpbjogMTU7XFxyXFxufVxcclxcblxcclxcbi5kZXNjcmlwdGlvbi1sYWJlbHtcXHJcXG4gICAgbWFyZ2luLWJvdHRvbTogMTU7XFxyXFxufVxcclxcblxcclxcbi5mYSB7XFxyXFxuXFx0Zm9udC1mYW1pbHk6IFxcXCJGb250QXdlc29tZSxmYS1zb2xpZC05MDBcXFwiO1xcclxcbn1cXHJcXG5cXHJcXG5SYWRTaWRlRHJhd2VyIC5kcmF3ZXJDb250ZW50VGV4dCB7XFxyXFxuICAgIGZvbnQtc2l6ZTogMTM7XFxyXFxuICAgIHBhZGRpbmc6IDEwO1xcclxcbn1cXHJcXG5cXHJcXG5SYWRTaWRlRHJhd2VyIC5kcmF3ZXJDb250ZW50QnV0dG9uIHtcXHJcXG4gICAgbWFyZ2luOiAxMDtcXHJcXG4gICAgaG9yaXpvbnRhbC1hbGlnbjogbGVmdDtcXHJcXG59XFxyXFxuXFxyXFxuLnNpZGVTdGFja0xheW91dCB7XFxyXFxuICAgIGJhY2tncm91bmQtY29sb3I6IGdyYXk7XFxyXFxufVxcclxcblxcclxcbi5zaWRlVGl0bGVTdGFja0xheW91dCB7XFxyXFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgdmVydGljYWwtYWxpZ246IGNlbnRlcjtcXHJcXG59XFxyXFxuXFxyXFxuLnNpZGVMYWJlbCB7XFxyXFxuICAgIHBhZGRpbmc6IDEwO1xcclxcbn1cXHJcXG5cXHJcXG4uc2lkZUxpZ2h0R3JheUxhYmVsIHtcXHJcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xcclxcbn1cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8VGFiVmlldyBhbmRyb2lkVGFic1Bvc2l0aW9uPVxcXCJ0b3BcXFwiPlxcclxcblxcdDxwYWdlLXJvdXRlci1vdXRsZXQgKnRhYkl0ZW09XFxcInt0aXRsZTogJ0F0dHJpYnV0ZXMnfVxcXCIgbmFtZT1cXFwiYXR0cmlidXRlc1RhYlxcXCI+XFxyXFxuXFx0PC9wYWdlLXJvdXRlci1vdXRsZXQ+XFxyXFxuXFxyXFxuXFx0PHBhZ2Utcm91dGVyLW91dGxldCAqdGFiSXRlbT1cXFwie3RpdGxlOiAnU2tpbGxzJ31cXFwiIG5hbWU9XFxcInNraWxsc1RhYlxcXCI+XFxyXFxuXFx0PC9wYWdlLXJvdXRlci1vdXRsZXQ+XFxyXFxuXFxyXFxuXFx0PHBhZ2Utcm91dGVyLW91dGxldCAqdGFiSXRlbT1cXFwie3RpdGxlOiAnUG93ZXJzJ31cXFwiIG5hbWU9XFxcInBvd2Vyc1RhYlxcXCI+XFxyXFxuXFx0PC9wYWdlLXJvdXRlci1vdXRsZXQ+XFxyXFxuXFxyXFxuXFx0PHBhZ2Utcm91dGVyLW91dGxldCAqdGFiSXRlbT1cXFwie3RpdGxlOiAnQ3liZXInfVxcXCIgbmFtZT1cXFwiY3liZXJUYWJcXFwiPlxcclxcblxcdDwvcGFnZS1yb3V0ZXItb3V0bGV0PlxcclxcbjwvVGFiVmlldz5cXHJcXG48VGFicz5cXHJcXG5cXHQ8IS0tIFRoZSBib3R0b20gdGFiIFVJIGlzIGNyZWF0ZWQgdmlhIFRhYlN0cmlwICh0aGUgY29udGFpbmllcikgYW5kIFRhYlN0cmlwSXRlbSAoZm9yIGVhY2ggdGFiKS0tPlxcclxcblxcdDxUYWJTdHJpcD5cXHJcXG5cXHRcXHQ8VGFiU3RyaXBJdGVtPlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJBdHRyaWJ1dGVzXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdDwvVGFiU3RyaXBJdGVtPlxcclxcblxcdFxcdDxUYWJTdHJpcEl0ZW0+XFxyXFxuXFx0XFx0XFx0PExhYmVsIHRleHQ9XFxcIlNraWxsc1xcXCI+PC9MYWJlbD5cXHJcXG5cXHRcXHQ8L1RhYlN0cmlwSXRlbT5cXHJcXG5cXHRcXHQ8VGFiU3RyaXBJdGVtPlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJQb3dlcnNcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0PC9UYWJTdHJpcEl0ZW0+XFxyXFxuXFx0XFx0PFRhYlN0cmlwSXRlbT5cXHJcXG5cXHRcXHRcXHQ8TGFiZWwgdGV4dD1cXFwiQ3liZXJcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0PC9UYWJTdHJpcEl0ZW0+XFxyXFxuXFx0PC9UYWJTdHJpcD5cXHJcXG5cXHJcXG5cXHQ8VGFiQ29udGVudEl0ZW0+XFxyXFxuXFx0XFx0PEdyaWRMYXlvdXQgcm93cz1cXFwiYXV0bywgKlxcXCI+XFxyXFxuXFx0XFx0XFx0PExhYmVsIHRleHQ9XFxcIkF0dHJpYnV0ZXNcXFwiIGNsYXNzPVxcXCJoMSB0ZXh0LWNlbnRlclxcXCIgcm93PVxcXCIwXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdFxcdDxMaXN0VmlldyBbaXRlbXNdPVxcXCJjaGFyYWN0ZXJBdHRyaWJ1dGVzXFxcIiBjbGFzcz1cXFwibGlzdC1ncm91cFxcXCIgW2l0ZW1UZW1wbGF0ZVNlbGVjdG9yXT1cXFwidGVtcGxhdGVTZWxlY3RvclxcXCJcXHJcXG5cXHRcXHRcXHRcXHRyb3c9XFxcIjFcXFwiPlxcclxcblxcdFxcdFxcdFxcdDxuZy10ZW1wbGF0ZSBuc1RlbXBsYXRlS2V5PVxcXCJyZWRcXFwiIGxldC1pdGVtPVxcXCJpdGVtXFxcIiBsZXQtaT1cXFwiaW5kZXhcXFwiPlxcclxcblxcdFxcdFxcdFxcdFxcdDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcIjIwMCwgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0PExhYmVsIHN0eWxlPVxcXCJwYWRkaW5nLWxlZnQ6NDBcXFwiIGNsYXNzPVxcXCIgaDJcXFwiIFt0ZXh0XT1cXFwiaXRlbS5OYW1lXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHJvdz1cXFwiaW5kZXhcXFwiXFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0Y29sdW1uPVxcXCIwXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdFxcdFxcdFxcdFxcdDxUZXh0RmllbGQgW3RleHRdPVxcXCJpdGVtLlNjb3JlXFxcIiBrZXlib2FyZFR5cGU9XFxcInBob25lXFxcIiBtYXhMZW5ndGg9XFxcIjJcXFwiIHJvdz1cXFwiaW5kZXhcXFwiIGNvbHVtbj1cXFwiMVxcXCIgc3R5bGU9XFxcIm1hcmdpbi1ib3R0b206MTU7IGZvbnQtc2l6ZToyNHB4OyB0ZXh0LWFsaWduOiBjZW50ZXI7XFxcIj48L1RleHRGaWVsZD5cXHRcXHRcXHRcXHRcXHQ8L0dyaWRMYXlvdXQ+XFxyXFxuXFx0XFx0XFx0XFx0PC9uZy10ZW1wbGF0ZT5cXHJcXG5cXHRcXHRcXHQ8L0xpc3RWaWV3PlxcclxcblxcdFxcdDwvR3JpZExheW91dD5cXHJcXG5cXHQ8L1RhYkNvbnRlbnRJdGVtPlxcclxcblxcdDxUYWJDb250ZW50SXRlbT5cXHJcXG5cXHRcXHQ8R3JpZExheW91dCByb3dzPVxcXCJhdXRvLCAqXFxcIj5cXHJcXG5cXHRcXHRcXHQ8TGFiZWwgdGV4dD1cXFwiU2tpbGxzXFxcIiBjbGFzcz1cXFwiaDEgdGV4dC1jZW50ZXJcXFwiIHJvdz1cXFwiMFxcXCI+PC9MYWJlbD5cXHJcXG5cXHRcXHRcXHQ8TGlzdFZpZXcgW2l0ZW1zXT1cXFwiY2hhcmFjdGVyU2tpbGxzXFxcIiBjbGFzcz1cXFwibGlzdC1ncm91cFxcXCIgW2l0ZW1UZW1wbGF0ZVNlbGVjdG9yXT1cXFwidGVtcGxhdGVTZWxlY3RvclxcXCJcXHJcXG5cXHRcXHRcXHRcXHRyb3c9XFxcIjFcXFwiPlxcclxcblxcdFxcdFxcdFxcdDxuZy10ZW1wbGF0ZSBuc1RlbXBsYXRlS2V5PVxcXCJyZWRcXFwiIGxldC1pdGVtPVxcXCJpdGVtXFxcIiBsZXQtaT1cXFwiaW5kZXhcXFwiPlxcclxcblxcdFxcdFxcdFxcdFxcdDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcIjIwMCwgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0PExhYmVsIHN0eWxlPVxcXCJwYWRkaW5nLWxlZnQ6NDBcXFwiIGNsYXNzPVxcXCIgaDJcXFwiIFt0ZXh0XT1cXFwiaXRlbS5OYW1lXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHJvdz1cXFwiaW5kZXhcXFwiXFxyXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0Y29sdW1uPVxcXCIwXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdFxcdFxcdFxcdFxcdDxUZXh0RmllbGQgW3RleHRdPVxcXCJpdGVtLlNjb3JlXFxcIiBrZXlib2FyZFR5cGU9XFxcInBob25lXFxcIiBtYXhMZW5ndGg9XFxcIjJcXFwiIHJvdz1cXFwiaW5kZXhcXFwiIGNvbHVtbj1cXFwiMVxcXCIgc3R5bGU9XFxcIm1hcmdpbi1ib3R0b206MTU7IGZvbnQtc2l6ZToyNHB4OyB0ZXh0LWFsaWduOiBjZW50ZXI7XFxcIj48L1RleHRGaWVsZD5cXHRcXHRcXHRcXHRcXHQ8L0dyaWRMYXlvdXQ+XFxyXFxuXFx0XFx0XFx0XFx0PC9uZy10ZW1wbGF0ZT5cXHJcXG5cXHRcXHRcXHQ8L0xpc3RWaWV3PlxcclxcblxcdFxcdDwvR3JpZExheW91dD5cXHJcXG5cXHQ8L1RhYkNvbnRlbnRJdGVtPlxcclxcblxcdDxUYWJDb250ZW50SXRlbT5cXHJcXG5cXHRcXHQ8R3JpZExheW91dD5cXHJcXG5cXHRcXHRcXHQ8TGFiZWwgdGV4dD1cXFwiUG93ZXJzXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuXFx0XFx0PC9HcmlkTGF5b3V0PlxcclxcblxcdDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuXFxyXFxuXFx0PFRhYkNvbnRlbnRJdGVtPlxcclxcblxcdFxcdDxHcmlkTGF5b3V0PlxcclxcblxcdFxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJDeWJlclxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcblxcdFxcdDwvR3JpZExheW91dD5cXHJcXG5cXHQ8L1RhYkNvbnRlbnRJdGVtPlxcclxcbjwvVGFicz5cXHJcXG5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBPbkluaXQsIEFmdGVyVmlld0luaXQsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQgfSBmcm9tICduYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyJztcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcblx0dGVtcGxhdGVVcmw6ICdob21lLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6IFsgJ2hvbWUuY29tcG9uZW50LmNzcycgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblx0cHJpdmF0ZSBfbWFpbkNvbnRlbnRUZXh0OiBzdHJpbmc7XHJcblxyXG5cdG1vZGlmaWVyOiBudW1iZXI7XHJcblx0aW5pdHZhbHVlOiBudW1iZXI7XHJcblx0cGxheWVyTmFtZTogc3RyaW5nO1xyXG5cdGNoYXJhY3RlckF0dHJpYnV0ZXM7XHJcblx0Y2hhcmFjdGVyU2tpbGxzO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIF9jaGFuZ2VEZXRlY3Rpb25SZWY6IENoYW5nZURldGVjdG9yUmVmKSB7XHJcblx0XHR0aGlzLnBsYXllck5hbWUgPSAnVG9kZCc7XHJcblx0XHR0aGlzLmNoYXJhY3RlclNraWxscyA9IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdBcmNoZXJ5JyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0F1dG9tYXRpY3MnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQmxhZGVzJyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0NsdWJzJyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0V4b3RpYyBSYW5nZWQgV2VhcG9uJyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0hlYXZ5IFdlYXBvbnMnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnTG9uZ2FybXMnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnUGlzdG9scycsXHJcblx0XHRcdFx0U2NvcmU6IDBcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdUaHJvd2luZyBXZWFwb25zJyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1VuYXJtZWQgQ29tYmF0JyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9XHJcblx0XHRdO1xyXG5cdFx0dGhpcy5jaGFyYWN0ZXJBdHRyaWJ1dGVzID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0JvZHknLFxyXG5cdFx0XHRcdFNjb3JlOiAxXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnU3RyZW5ndGgnLFxyXG5cdFx0XHRcdFNjb3JlOiAxXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQWdpbGl0eScsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdSZWFjdGlvbicsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdMb2dpYycsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdXaWxscG93ZXInLFxyXG5cdFx0XHRcdFNjb3JlOiAxXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnSW50dWl0aW9uJyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0NoYXJpc21hJyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0Vzc2VuY2UnLFxyXG5cdFx0XHRcdFNjb3JlOiA2XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRWRnZScsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdF07XHJcblxyXG5cdFx0dGhpcy5tb2RpZmllciA9IHRoaXMuZ2V0TW9kaWZpZXIoKTtcclxuXHR9XHJcblxyXG5cdEBWaWV3Q2hpbGQoUmFkU2lkZURyYXdlckNvbXBvbmVudCwgeyBzdGF0aWM6IGZhbHNlIH0pXHJcblx0cHVibGljIGRyYXdlckNvbXBvbmVudDogUmFkU2lkZURyYXdlckNvbXBvbmVudDtcclxuXHRwcml2YXRlIGRyYXdlcjogUmFkU2lkZURyYXdlcjtcclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7fVxyXG5cclxuXHRvbkJ1dHRvblRhcCgpOiB2b2lkIHtcclxuXHRcdHRoaXMubW9kaWZpZXIgPSB0eXBlb2YgdGhpcy5tb2RpZmllciA9PT0gJ3VuZGVmaW5lZCcgPyAwIDogdGhpcy5tb2RpZmllcjtcclxuXHRcdHRoaXMuaW5pdHZhbHVlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogTWF0aC5mbG9vcig2KSkgKyAxICsgTnVtYmVyKHRoaXMubW9kaWZpZXIpO1xyXG5cdH1cclxuXHJcblx0Z2V0TW9kaWZpZXIoKSB7XHJcblx0XHR2YXIgcmVhY3Rpb24gPSB0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMuZmluZChvYmogPT4ge1xyXG5cdFx0XHQgIHJldHVybiBvYmouTmFtZSA9PSAnUmVhY3Rpb24nXHJcblx0XHR9KTtcclxuXHJcblx0XHR2YXIgaW50dWl0aW9uID0gdGhpcy5jaGFyYWN0ZXJBdHRyaWJ1dGVzLmZpbmQob2JqID0+IHtcclxuXHRcdFx0ICByZXR1cm4gb2JqLk5hbWUgPT0gJ0ludHVpdGlvbidcclxuXHRcdH0pO1xyXG5cclxuXHRcdHJldHVybiByZWFjdGlvbi5TY29yZSArIGludHVpdGlvbi5TY29yZTtcclxuXHR9XHJcbn1cclxuIiwiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFVJU2lkZURyYXdlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFVJTGlzdFZpZXdNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3L2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlDYWxlbmRhck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktY2FsZW5kYXIvYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUNoYXJ0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1jaGFydC9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFVJRGF0YUZvcm1Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLWRhdGFmb3JtL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlBdXRvQ29tcGxldGVUZXh0Vmlld01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktYXV0b2NvbXBsZXRlL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlHYXVnZU1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktZ2F1Z2UvYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuaW1wb3J0IHsgSG9tZVJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9ob21lLXJvdXRpbmcubW9kdWxlXCI7XHJcbmltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tIFwiLi9ob21lLmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRVSVNpZGVEcmF3ZXJNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0VUlMaXN0Vmlld01vZHVsZSxcclxuICAgICAgICBOYXRpdmVTY3JpcHRVSUNhbGVuZGFyTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdFVJQ2hhcnRNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0VUlEYXRhRm9ybU1vZHVsZSxcclxuICAgICAgICBOYXRpdmVTY3JpcHRVSUF1dG9Db21wbGV0ZVRleHRWaWV3TW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdFVJR2F1Z2VNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgICAgIEhvbWVSb3V0aW5nTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgSG9tZUNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb21lTW9kdWxlIHsgfVxyXG4iXSwic291cmNlUm9vdCI6IiJ9