webpackHotUpdate(0,{

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Longarms',
                Score: 5
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 4
            },
            {
                Name: 'Strength',
                Score: 2
            },
            {
                Name: 'Agility',
                Score: 5
            },
            {
                Name: 'Reaction',
                Score: 5
            },
            {
                Name: 'Logic',
                Score: 6
            },
            {
                Name: 'Willpower',
                Score: 2
            },
            {
                Name: 'Intuition',
                Score: 3
            },
            {
                Name: 'Charisma',
                Score: 2
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 2
            },
        ];
        this.modifier = this.getModifer();
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.prototype.getModifer = function () {
        var reaction = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
        var intuition = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Intuition';
        });
        return reaction + intuition;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1NBQ0QsQ0FBQztRQUNGLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUMxQjtnQkFDQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxNQUFNO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1I7U0FDRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQU1ELGdDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQixtQ0FBVyxHQUFYO1FBQ0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLElBQUksQ0FBQyxRQUFRLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFDQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQUc7WUFDN0MsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFVBQVU7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQUc7WUFDOUMsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFdBQVc7UUFDakMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLFFBQVEsR0FBRyxTQUFTLENBQUM7SUFDN0IsQ0FBQzs7Z0JBekV3QywrREFBaUI7O0lBc0QxRDtRQURDLCtEQUFTLENBQUMseUZBQXNCLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7a0NBQzdCLHlGQUFzQjswREFBQztJQS9EbkMsYUFBYTtRQUx6QiwrREFBUyxDQUFDO1lBRVYsb0ZBQWtDOztTQUVsQyxDQUFDO3lDQVV3QywrREFBaUI7T0FUOUMsYUFBYSxDQW1GekI7SUFBRCxvQkFBQztDQUFBO0FBbkZ5QiIsImZpbGUiOiIwLjE4YThjMmY2NjM2MTllMjBkMGE0LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgT25Jbml0LCBBZnRlclZpZXdJbml0LCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyQ29tcG9uZW50IH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXIvYW5ndWxhcic7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXIgfSBmcm9tICduYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHRlbXBsYXRlVXJsOiAnaG9tZS5jb21wb25lbnQuaHRtbCcsXHJcblx0c3R5bGVVcmxzOiBbICdob21lLmNvbXBvbmVudC5jc3MnIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cdHByaXZhdGUgX21haW5Db250ZW50VGV4dDogc3RyaW5nO1xyXG5cclxuXHRtb2RpZmllcjogbnVtYmVyO1xyXG5cdGluaXR2YWx1ZTogbnVtYmVyO1xyXG5cdHBsYXllck5hbWU6IHN0cmluZztcclxuXHRjaGFyYWN0ZXJBdHRyaWJ1dGVzO1xyXG5cdGNoYXJhY3RlclNraWxscztcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBfY2hhbmdlRGV0ZWN0aW9uUmVmOiBDaGFuZ2VEZXRlY3RvclJlZikge1xyXG5cdFx0dGhpcy5wbGF5ZXJOYW1lID0gJ1RvZGQnO1xyXG5cdFx0dGhpcy5jaGFyYWN0ZXJTa2lsbHMgPSBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnTG9uZ2FybXMnLFxyXG5cdFx0XHRcdFNjb3JlOiA1XHJcblx0XHRcdH1cclxuXHRcdF07XHJcblx0XHR0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMgPSBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQm9keScsXHJcblx0XHRcdFx0U2NvcmU6IDRcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdTdHJlbmd0aCcsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdBZ2lsaXR5JyxcclxuXHRcdFx0XHRTY29yZTogNVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1JlYWN0aW9uJyxcclxuXHRcdFx0XHRTY29yZTogNVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0xvZ2ljJyxcclxuXHRcdFx0XHRTY29yZTogNlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1dpbGxwb3dlcicsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdJbnR1aXRpb24nLFxyXG5cdFx0XHRcdFNjb3JlOiAzXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQ2hhcmlzbWEnLFxyXG5cdFx0XHRcdFNjb3JlOiAyXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRXNzZW5jZScsXHJcblx0XHRcdFx0U2NvcmU6IDZcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdFZGdlJyxcclxuXHRcdFx0XHRTY29yZTogMlxyXG5cdFx0XHR9LFxyXG5cdFx0XTtcclxuXHRcdHRoaXMubW9kaWZpZXIgPSB0aGlzLmdldE1vZGlmZXIoKTtcclxuXHR9XHJcblxyXG5cdEBWaWV3Q2hpbGQoUmFkU2lkZURyYXdlckNvbXBvbmVudCwgeyBzdGF0aWM6IGZhbHNlIH0pXHJcblx0cHVibGljIGRyYXdlckNvbXBvbmVudDogUmFkU2lkZURyYXdlckNvbXBvbmVudDtcclxuXHRwcml2YXRlIGRyYXdlcjogUmFkU2lkZURyYXdlcjtcclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7fVxyXG5cclxuXHRvbkJ1dHRvblRhcCgpOiB2b2lkIHtcclxuXHRcdHRoaXMubW9kaWZpZXIgPSB0eXBlb2YgdGhpcy5tb2RpZmllciA9PT0gJ3VuZGVmaW5lZCcgPyAwIDogdGhpcy5tb2RpZmllcjtcclxuXHRcdHRoaXMuaW5pdHZhbHVlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogTWF0aC5mbG9vcig2KSkgKyAxICsgTnVtYmVyKHRoaXMubW9kaWZpZXIpO1xyXG5cdH1cclxuXHJcblx0Z2V0TW9kaWZlcigpIHtcclxuXHRcdHZhciByZWFjdGlvbiA9IHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcy5maW5kKG9iaiA9PiB7XHJcblx0XHRcdCAgcmV0dXJuIG9iai5OYW1lID09ICdSZWFjdGlvbidcclxuXHRcdH0pO1xyXG5cclxuXHRcdHZhciBpbnR1aXRpb24gPSB0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMuZmluZChvYmogPT4ge1xyXG5cdFx0XHQgIHJldHVybiBvYmouTmFtZSA9PSAnSW50dWl0aW9uJ1xyXG5cdFx0fSk7XHJcblx0XHRyZXR1cm4gcmVhY3Rpb24gKyBpbnR1aXRpb247XHJcblx0fVxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=