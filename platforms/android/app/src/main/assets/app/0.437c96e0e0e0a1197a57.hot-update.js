webpackHotUpdate(0,{

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Archery',
                Score: 0
            },
            {
                Name: 'Automatics',
                Score: 0
            },
            {
                Name: 'Blades',
                Score: 0
            },
            {
                Name: 'Clubs',
                Score: 0
            },
            {
                Name: 'Exotic Ranged Weapon',
                Score: 0
            },
            {
                Name: 'Heavy Weapons',
                Score: 0
            },
            {
                Name: 'Longarms',
                Score: 0
            },
            {
                Name: 'Pistols',
                Score: 0
            },
            {
                Name: 'Throwing Weapons',
                Score: 0
            },
            {
                Name: 'Unarmed Combat',
                Score: 0
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 1
            },
            {
                Name: 'Strength',
                Score: 1
            },
            {
                Name: 'Agility',
                Score: 1
            },
            {
                Name: 'Reaction',
                Score: 1
            },
            {
                Name: 'Logic',
                Score: 1
            },
            {
                Name: 'Willpower',
                Score: 1
            },
            {
                Name: 'Intuition',
                Score: 1
            },
            {
                Name: 'Charisma',
                Score: 1
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 1
            },
        ];
        this.modifier = this.getModifier();
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.prototype.getModifier = function () {
        var reaction = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
        var intuition = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Intuition';
        });
        return reaction.Score + intuition.Score;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxRQUFRO2dCQUNkLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxlQUFlO2dCQUNyQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLGtCQUFrQjtnQkFDeEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLEtBQUssRUFBRSxDQUFDO2FBQ1I7U0FDRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQzFCO2dCQUNDLElBQUksRUFBRSxNQUFNO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLE1BQU07Z0JBQ1osS0FBSyxFQUFFLENBQUM7YUFDUjtTQUNELENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBTUQsZ0NBQVEsR0FBUixjQUFrQixDQUFDO0lBRW5CLG1DQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sSUFBSSxDQUFDLFFBQVEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRUQsbUNBQVcsR0FBWDtRQUNDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM3QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksVUFBVTtRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM5QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksV0FBVztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sUUFBUSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ3pDLENBQUM7O2dCQS9Hd0MsK0RBQWlCOztJQTJGMUQ7UUFEQywrREFBUyxDQUFDLHlGQUFzQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2tDQUM3Qix5RkFBc0I7MERBQUM7SUFwR25DLGFBQWE7UUFMekIsK0RBQVMsQ0FBQztZQUVWLG9GQUFrQzs7U0FFbEMsQ0FBQzt5Q0FVd0MsK0RBQWlCO09BVDlDLGFBQWEsQ0F5SHpCO0lBQUQsb0JBQUM7Q0FBQTtBQXpIeUIiLCJmaWxlIjoiMC40MzdjOTZlMGUwZTBhMTE5N2E1Ny5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXInO1xyXG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyIH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogJ2hvbWUuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWyAnaG9tZS5jb21wb25lbnQuY3NzJyBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRwcml2YXRlIF9tYWluQ29udGVudFRleHQ6IHN0cmluZztcclxuXHJcblx0bW9kaWZpZXI6IG51bWJlcjtcclxuXHRpbml0dmFsdWU6IG51bWJlcjtcclxuXHRwbGF5ZXJOYW1lOiBzdHJpbmc7XHJcblx0Y2hhcmFjdGVyQXR0cmlidXRlcztcclxuXHRjaGFyYWN0ZXJTa2lsbHM7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgX2NoYW5nZURldGVjdGlvblJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYpIHtcclxuXHRcdHRoaXMucGxheWVyTmFtZSA9ICdUb2RkJztcclxuXHRcdHRoaXMuY2hhcmFjdGVyU2tpbGxzID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0FyY2hlcnknLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQXV0b21hdGljcycsXHJcblx0XHRcdFx0U2NvcmU6IDBcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdCbGFkZXMnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQ2x1YnMnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRXhvdGljIFJhbmdlZCBXZWFwb24nLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnSGVhdnkgV2VhcG9ucycsXHJcblx0XHRcdFx0U2NvcmU6IDBcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdMb25nYXJtcycsXHJcblx0XHRcdFx0U2NvcmU6IDBcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdQaXN0b2xzJyxcclxuXHRcdFx0XHRTY29yZTogMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1Rocm93aW5nIFdlYXBvbnMnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnVW5hcm1lZCBDb21iYXQnLFxyXG5cdFx0XHRcdFNjb3JlOiAwXHJcblx0XHRcdH1cclxuXHRcdF07XHJcblx0XHR0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMgPSBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQm9keScsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdTdHJlbmd0aCcsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdBZ2lsaXR5JyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1JlYWN0aW9uJyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0xvZ2ljJyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1dpbGxwb3dlcicsXHJcblx0XHRcdFx0U2NvcmU6IDFcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdJbnR1aXRpb24nLFxyXG5cdFx0XHRcdFNjb3JlOiAxXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnQ2hhcmlzbWEnLFxyXG5cdFx0XHRcdFNjb3JlOiAxXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnRXNzZW5jZScsXHJcblx0XHRcdFx0U2NvcmU6IDZcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdFZGdlJyxcclxuXHRcdFx0XHRTY29yZTogMVxyXG5cdFx0XHR9LFxyXG5cdFx0XTtcclxuXHJcblx0XHR0aGlzLm1vZGlmaWVyID0gdGhpcy5nZXRNb2RpZmllcigpO1xyXG5cdH1cclxuXHJcblx0QFZpZXdDaGlsZChSYWRTaWRlRHJhd2VyQ29tcG9uZW50LCB7IHN0YXRpYzogZmFsc2UgfSlcclxuXHRwdWJsaWMgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xyXG5cdHByaXZhdGUgZHJhd2VyOiBSYWRTaWRlRHJhd2VyO1xyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG5cdG9uQnV0dG9uVGFwKCk6IHZvaWQge1xyXG5cdFx0dGhpcy5tb2RpZmllciA9IHR5cGVvZiB0aGlzLm1vZGlmaWVyID09PSAndW5kZWZpbmVkJyA/IDAgOiB0aGlzLm1vZGlmaWVyO1xyXG5cdFx0dGhpcy5pbml0dmFsdWUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBNYXRoLmZsb29yKDYpKSArIDEgKyBOdW1iZXIodGhpcy5tb2RpZmllcik7XHJcblx0fVxyXG5cclxuXHRnZXRNb2RpZmllcigpIHtcclxuXHRcdHZhciByZWFjdGlvbiA9IHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcy5maW5kKG9iaiA9PiB7XHJcblx0XHRcdCAgcmV0dXJuIG9iai5OYW1lID09ICdSZWFjdGlvbidcclxuXHRcdH0pO1xyXG5cclxuXHRcdHZhciBpbnR1aXRpb24gPSB0aGlzLmNoYXJhY3RlckF0dHJpYnV0ZXMuZmluZChvYmogPT4ge1xyXG5cdFx0XHQgIHJldHVybiBvYmouTmFtZSA9PSAnSW50dWl0aW9uJ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0cmV0dXJuIHJlYWN0aW9uLlNjb3JlICsgaW50dWl0aW9uLlNjb3JlO1xyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9