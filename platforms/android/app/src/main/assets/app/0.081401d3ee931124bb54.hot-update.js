webpackHotUpdate(0,{

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-ui-sidedrawer/angular");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__);


var HomeComponent = /** @class */ (function () {
    function HomeComponent(_changeDetectionRef) {
        this._changeDetectionRef = _changeDetectionRef;
        this.playerName = 'Todd';
        this.characterSkills = [
            {
                Name: 'Longarms',
                Score: 5
            }
        ];
        this.characterAttributes = [
            {
                Name: 'Body',
                Score: 4
            },
            {
                Name: 'Strength',
                Score: 2
            },
            {
                Name: 'Agility',
                Score: 5
            },
            {
                Name: 'Reaction',
                Score: 5
            },
            {
                Name: 'Logic',
                Score: 6
            },
            {
                Name: 'Willpower',
                Score: 2
            },
            {
                Name: 'Intuition',
                Score: 3
            },
            {
                Name: 'Charisma',
                Score: 2
            },
            {
                Name: 'Essence',
                Score: 6
            },
            {
                Name: 'Edge',
                Score: 2
            },
        ];
        this.modifier = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.onButtonTap = function () {
        this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
        this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
    };
    HomeComponent.prototype.getModifer = function () {
        var reaction = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Reaction';
        });
        var intuition = this.characterAttributes.find(function (obj) {
            return obj.Name == 'Intuition';
        });
        return reaction + intuition;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"], { static: false }),
        __metadata("design:type", nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_1__["RadSideDrawerComponent"])
    ], HomeComponent.prototype, "drawerComponent", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __importDefault(__webpack_require__("./home/home.component.html")).default,
            styles: [__importDefault(__webpack_require__("./home/home.component.css")).default]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUNuQjtBQVE1RTtJQVNDLHVCQUFvQixtQkFBc0M7UUFBdEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFtQjtRQUN6RCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3RCO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1NBQ0QsQ0FBQztRQUNGLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUMxQjtnQkFDQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxDQUFDO2FBQ1I7WUFDRDtnQkFDQyxJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNEO2dCQUNDLElBQUksRUFBRSxNQUFNO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1I7U0FDRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQUc7WUFDOUMsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFVBQVU7UUFDaEMsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBTUQsZ0NBQVEsR0FBUixjQUFrQixDQUFDO0lBRW5CLG1DQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sSUFBSSxDQUFDLFFBQVEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRUQsa0NBQVUsR0FBVjtRQUNDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM3QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksVUFBVTtRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBRztZQUM5QyxPQUFPLEdBQUcsQ0FBQyxJQUFJLElBQUksV0FBVztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sUUFBUSxHQUFHLFNBQVMsQ0FBQztJQUM3QixDQUFDOztnQkEzRXdDLCtEQUFpQjs7SUF3RDFEO1FBREMsK0RBQVMsQ0FBQyx5RkFBc0IsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztrQ0FDN0IseUZBQXNCOzBEQUFDO0lBakVuQyxhQUFhO1FBTHpCLCtEQUFTLENBQUM7WUFFVixvRkFBa0M7O1NBRWxDLENBQUM7eUNBVXdDLCtEQUFpQjtPQVQ5QyxhQUFhLENBcUZ6QjtJQUFELG9CQUFDO0NBQUE7QUFyRnlCIiwiZmlsZSI6IjAuMDgxNDAxZDNlZTkzMTEyNGJiNTQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBPbkluaXQsIEFmdGVyVmlld0luaXQsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQgfSBmcm9tICduYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyJztcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcblx0dGVtcGxhdGVVcmw6ICdob21lLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6IFsgJ2hvbWUuY29tcG9uZW50LmNzcycgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblx0cHJpdmF0ZSBfbWFpbkNvbnRlbnRUZXh0OiBzdHJpbmc7XHJcblxyXG5cdG1vZGlmaWVyOiBudW1iZXI7XHJcblx0aW5pdHZhbHVlOiBudW1iZXI7XHJcblx0cGxheWVyTmFtZTogc3RyaW5nO1xyXG5cdGNoYXJhY3RlckF0dHJpYnV0ZXM7XHJcblx0Y2hhcmFjdGVyU2tpbGxzO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIF9jaGFuZ2VEZXRlY3Rpb25SZWY6IENoYW5nZURldGVjdG9yUmVmKSB7XHJcblx0XHR0aGlzLnBsYXllck5hbWUgPSAnVG9kZCc7XHJcblx0XHR0aGlzLmNoYXJhY3RlclNraWxscyA9IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdMb25nYXJtcycsXHJcblx0XHRcdFx0U2NvcmU6IDVcclxuXHRcdFx0fVxyXG5cdFx0XTtcclxuXHRcdHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcyA9IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdCb2R5JyxcclxuXHRcdFx0XHRTY29yZTogNFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ1N0cmVuZ3RoJyxcclxuXHRcdFx0XHRTY29yZTogMlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0FnaWxpdHknLFxyXG5cdFx0XHRcdFNjb3JlOiA1XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnUmVhY3Rpb24nLFxyXG5cdFx0XHRcdFNjb3JlOiA1XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnTG9naWMnLFxyXG5cdFx0XHRcdFNjb3JlOiA2XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHROYW1lOiAnV2lsbHBvd2VyJyxcclxuXHRcdFx0XHRTY29yZTogMlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0ludHVpdGlvbicsXHJcblx0XHRcdFx0U2NvcmU6IDNcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdDaGFyaXNtYScsXHJcblx0XHRcdFx0U2NvcmU6IDJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdE5hbWU6ICdFc3NlbmNlJyxcclxuXHRcdFx0XHRTY29yZTogNlxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0TmFtZTogJ0VkZ2UnLFxyXG5cdFx0XHRcdFNjb3JlOiAyXHJcblx0XHRcdH0sXHJcblx0XHRdO1xyXG5cdFx0dGhpcy5tb2RpZmllciA9IHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcy5maW5kKG9iaiA9PiB7XHJcblx0XHRcdCAgcmV0dXJuIG9iai5OYW1lID09ICdSZWFjdGlvbidcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0QFZpZXdDaGlsZChSYWRTaWRlRHJhd2VyQ29tcG9uZW50LCB7IHN0YXRpYzogZmFsc2UgfSlcclxuXHRwdWJsaWMgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xyXG5cdHByaXZhdGUgZHJhd2VyOiBSYWRTaWRlRHJhd2VyO1xyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG5cdG9uQnV0dG9uVGFwKCk6IHZvaWQge1xyXG5cdFx0dGhpcy5tb2RpZmllciA9IHR5cGVvZiB0aGlzLm1vZGlmaWVyID09PSAndW5kZWZpbmVkJyA/IDAgOiB0aGlzLm1vZGlmaWVyO1xyXG5cdFx0dGhpcy5pbml0dmFsdWUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBNYXRoLmZsb29yKDYpKSArIDEgKyBOdW1iZXIodGhpcy5tb2RpZmllcik7XHJcblx0fVxyXG5cclxuXHRnZXRNb2RpZmVyKCkge1xyXG5cdFx0dmFyIHJlYWN0aW9uID0gdGhpcy5jaGFyYWN0ZXJBdHRyaWJ1dGVzLmZpbmQob2JqID0+IHtcclxuXHRcdFx0ICByZXR1cm4gb2JqLk5hbWUgPT0gJ1JlYWN0aW9uJ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0dmFyIGludHVpdGlvbiA9IHRoaXMuY2hhcmFjdGVyQXR0cmlidXRlcy5maW5kKG9iaiA9PiB7XHJcblx0XHRcdCAgcmV0dXJuIG9iai5OYW1lID09ICdJbnR1aXRpb24nXHJcblx0XHR9KTtcclxuXHRcdHJldHVybiByZWFjdGlvbiArIGludHVpdGlvbjtcclxuXHR9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==