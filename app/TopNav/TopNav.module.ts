import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { TopNavRoutingModule } from "./TopNav-routing.module";
import { TopNavComponent } from "./TopNav.component";

@NgModule({
    imports: [
        NativeScriptModule,
        TopNavRoutingModule
    ],
    declarations: [
        TopNavComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TopNavModule { }
