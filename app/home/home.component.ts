import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
	moduleId: module.id,
	templateUrl: 'home.component.html',
	styleUrls: [ 'home.component.css' ]
})
export class HomeComponent implements OnInit {
	private _mainContentText: string;

	modifier: number;
	initvalue: number;
	playerName: string;
	characterAttributes;
	characterSkills;

	constructor(private _changeDetectionRef: ChangeDetectorRef) {
		this.playerName = 'Todd';
		this.characterSkills = [
			{
				Name: 'Archery',
				Score: 0
			},
			{
				Name: 'Automatics',
				Score: 0
			},
			{
				Name: 'Blades',
				Score: 0
			},
			{
				Name: 'Clubs',
				Score: 0
			},
			{
				Name: 'Exotic Ranged Weapon',
				Score: 0
			},
			{
				Name: 'Heavy Weapons',
				Score: 0
			},
			{
				Name: 'Longarms',
				Score: 0
			},
			{
				Name: 'Pistols',
				Score: 0
			},
			{
				Name: 'Throwing Weapons',
				Score: 0
			},
			{
				Name: 'Unarmed Combat',
				Score: 0
			},
			{
				Name: 'Disguise',
				Score: 0
			},
			{
				Name: 'Diving',
				Score: 0
			},
			{
				Name: 'Escape Artist',
				Score: 0
			},
			{
				Name: 'Free Fall',
				Score: 0
			},
			{
				Name: 'Gymnastics',
				Score: 0
			},
			{
				Name: 'Palming',
				Score: 0
			},
			{
				Name: 'Perception',
				Score: 0
			},
			{
				Name: 'Running',
				Score: 0
			},
			{
				Name: 'Sneaking',
				Score: 0
			},
			{
				Name: 'Survival',
				Score: 0
			},
			{
				Name: 'Swimming',
				Score: 0
			},
			{
				Name: 'Tracking',
				Score: 0
			},
			{
				Name: 'Con',
				Score: 0
			},
			{
				Name: 'Ettiquette',
				Score: 0
			},
			{
				Name: 'Impersonation',
				Score: 0
			},
			{
				Name: 'Instruction',
				Score: 0
			},
			{
				Name: 'Intimidation',
				Score: 0
			},
			{
				Name: 'Leadership',
				Score: 0
			},
			{
				Name: 'Negotion',
				Score: 0
			},
			{
				Name: 'Performance',
				Score: 0
			},
			{
				Name: 'Alchemy',
				Score: 0
			},
			{
				Name: 'Arcana',
				Score: 0
			},
			{
				Name: 'Artificing',
				Score: 0
			},			
			{
				Name: 'Assensing',
				Score: 0
			},
			{
				Name: 'Astral Combat',
				Score: 0
			},
			{
				Name: 'Banishing',
				Score: 0
			},
			{
				Name: 'Binding',
				Score: 0
			},
			{
				Name: 'Counterspelling',
				Score: 0
			},
			{
				Name: 'Disenchanting',
				Score: 0
			},
			{
				Name: 'Ritual Spellcasting',
				Score: 0
			},
			{
				Name: 'Spellcasting',
				Score: 0
			},
			{
				Name: 'Summoning',
				Score: 0
			},
			{
				Name: 'Compiling',
				Score: 0
			},
			{
				Name: 'Decompiling',
				Score: 0
			},
			{
				Name: 'Registering',
				Score: 0
			},
			{
				Name: 'Aeronautics Mechanic',
				Score: 0
			},
			{
				Name: 'Animal Handling',
				Score: 0
			},
			{
				Name: 'Armorer',
				Score: 0
			},
			{
				Name: 'Artisan',
				Score: 0
			},
			{
				Name: 'Automotive Mechanic',
				Score: 0
			},
			{
				Name: 'Biotechnology',
				Score: 0
			},
			{
				Name: 'Chemistry',
				Score: 0
			},
			{
				Name: 'Computer',
				Score: 0
			},
			{
				Name: 'Demolitions',
				Score: 0
			},
			{
				Name: 'Electronic Warfare',
				Score: 0
			},
			{
				Name: 'First Aid',
				Score: 0
			},
			{
				Name: 'Forgery',
				Score: 0
			},
			{
				Name: 'Hacking',
				Score: 0
			},
			{
				Name: 'Hardware',
				Score: 0
			},
			{
				Name: 'Industrial Mechanic',
				Score: 0
			},
			{
				Name: 'Locksmith',
				Score: 0
			},
			{
				Name: 'Medicine',
				Score: 0
			},
			{
				Name: 'Nautical Mechanic',
				Score: 0
			},
			{
				Name: 'Navigation',
				Score: 0
			},
			{
				Name: 'Software',
				Score: 0
			},
			{
				Name: 'Gunnery',
				Score: 0
			},
			{
				Name: 'Pilot Aerospace',
				Score: 0
			},
			{
				Name: 'Pilot Aircraft',
				Score: 0
			},
			{
				Name: 'Pilot Walker',
				Score: 0
			},
			{
				Name: 'Pilot Exotic Vehicle',
				Score: 0
			},
			{
				Name: 'Pilot Groundcraft',
				Score: 0
			},
			{
				Name: 'Pilot Watercraft',
				Score: 0
			},
		];
		this.characterAttributes = [
			{
				Name: 'Body',
				Score: 1
			},
			{
				Name: 'Strength',
				Score: 1
			},
			{
				Name: 'Agility',
				Score: 1
			},
			{
				Name: 'Reaction',
				Score: 1
			},
			{
				Name: 'Logic',
				Score: 1
			},
			{
				Name: 'Willpower',
				Score: 1
			},
			{
				Name: 'Intuition',
				Score: 1
			},
			{
				Name: 'Charisma',
				Score: 1
			},
			{
				Name: 'Essence',
				Score: 6
			},
			{
				Name: 'Edge',
				Score: 1
			},
		];

		this.modifier = this.getModifier();
	}

	@ViewChild(RadSideDrawerComponent, { static: false })
	public drawerComponent: RadSideDrawerComponent;
	private drawer: RadSideDrawer;

	ngOnInit(): void {}

	onButtonTap(): void {
		this.modifier = typeof this.modifier === 'undefined' ? 0 : this.modifier;
		this.initvalue = Math.floor(Math.random() * Math.floor(6)) + 1 + Number(this.modifier);
	}

	getModifier() {
		var reaction = this.characterAttributes.find(obj => {
			  return obj.Name == 'Reaction'
		});

		var intuition = this.characterAttributes.find(obj => {
			  return obj.Name == 'Intuition'
		});

		return reaction.Score + intuition.Score;
	}
}
