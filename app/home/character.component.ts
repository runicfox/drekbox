import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
	moduleId: module.id,
	templateUrl: 'home.component.html',
	styleUrls: ['home.component.css']
})
export class PlayerComponent implements OnInit {
	private _mainContentText: string;

	modifier: number;
	initvalue: number;
	characterName;
	characterAttributes;

	constructor(private _changeDetectionRef: ChangeDetectorRef) {
		this.characterName = "Foin Boi";
		this.characterAttributes = [
			{
				Attribute: 'Body',
				Score: 4
			},
			{
				Attribute: 'Strength',
				Score: 2
			},
			{
				Attribute: 'Agility',
				Score: 5
			},
			{
				Attribute: 'Logic',
				Score: 6
			},
			{
				Attribute: 'Willpower',
				Score: 2
			},
			{
				Attribute: 'Intuition',
				Score: 3
			},
			{
				Attribute: 'Charisma',
				Score: 2
			},
			{
				Attribute: 'Essence',
				Score: 6
			},
			{
				Attribute: 'Edge',
				Score: 2
			}
		];
	}

	ngOnInit(): void { }
}



